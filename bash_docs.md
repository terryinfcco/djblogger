Welcome to the djblogger wiki!

Plan is to use this to document the bash commands that go along with the main part of the repo

### Video 27

- Main folder is in /home/terry/git/djblogger and it's on git as terryinfcco/djblogger
- Changed the font size of the terminal in vscode to 18.
- ctrl-shift-backtick opens the terminal in vscode.
- ~/git/djblogger$ python -m venv venv
- ~/git/djblogger$ source venv/bin/activate
- (venv) terry@kub2110:~/git/djblogger$ pip install django
- (venv) terry@kub2110:~/git/djblogger$ django-admin startproject djblogger
- (venv) terry@kub2110:~/git/djblogger/djblogger$ ./manage.py runserver 8001
- 
